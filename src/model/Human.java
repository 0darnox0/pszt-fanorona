package model;

import view.FxBoardController;

import java.util.concurrent.CountDownLatch;

public class Human extends Player {
    private FxBoardController controller;

    public Human(Logic logic, FxBoardController controller, Color color) {
        super(logic, color);
        this.controller = controller;
    }

    /**
     * @return move selected by human player
     */
    @Override
    public GameMove selectMove() {
        //System.out.println("Human@" + getColor() + " makes move");

        CountDownLatch waiter = new CountDownLatch(1); // Used to wait until player choose a move in GUI

        controller.selectMove(waiter);

        try {
            waiter.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return controller.getMove();
    }

    @Override
    public String toString() {
        return "Human";
    }
}
