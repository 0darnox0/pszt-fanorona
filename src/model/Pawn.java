package model;

public class Pawn {
    private final Color color;

    public Pawn(Color c) {
        color = c;
    }

    public Color getColor() {
        return color;
    }
}
