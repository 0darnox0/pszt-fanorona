package model;

import algorithm.MinMax;

import java.util.concurrent.TimeUnit;

public class Computer extends Player {
    private static final double MIN_MOVE_TIME = 2;  // in seconds, sets to better see the game course
    private MinMax algorithm;
    private long totalMovesTime = 0;
    private long movesCount = 0;

    public Computer(Logic logic, Color color) {
        super(logic, color);
        algorithm = new MinMax(logic, color);
    }

    /**
     * Sets depth of search in tree build by MinMax
     * @param maxDepth
     */
    public void setMinMaxDepth(int maxDepth) {
        this.algorithm.setMaxDepth(maxDepth);
    }

    @Override
    public GameMove selectMove() {
        //System.out.println("Computer@" + getColor() + " makes move");

        long startTime = System.nanoTime();
        GameMove move = algorithm.chooseMove();
        long endTime = System.nanoTime();

        if (endTime - startTime < MIN_MOVE_TIME * 1e9) {
            try {
                TimeUnit.NANOSECONDS.sleep((long) (MIN_MOVE_TIME * 1e9 - (endTime - startTime)));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        totalMovesTime += (endTime - startTime);
        movesCount += 1;

        return move;
    }

    /**
     * @return average time which takes to search through tree and select move
     */
    public double getAverageMoveTime() {
        return totalMovesTime / (double) movesCount / 1e9;
    }

    public long getMovesCount() {
        return movesCount;
    }

    @Override
    public String toString() {
        return "Computer";
    }
}
