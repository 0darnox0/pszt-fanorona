package model;

import java.util.ArrayList;
import java.util.Optional;


public class Logic {
    private State state;
    private Board board;

    public Logic() {
        state = new State();
        board = new Board();
    }

    /**
     * @return whose turn is now
     */
    public synchronized Color getTurn() {
        return state.getTurn();
    }

    /**
     * @return all possible moves in a current turn for current player
     */
    public synchronized ArrayList<GameMove> legalMoves() {
        return legalMoves(state.getTurn());
    }

    public synchronized void makeMove(GameMove move) {
        if (!isMoveLegal(move)) throw new RuntimeException("Illegal move");

        state.addMove(move);
        if (move instanceof PawnMove)
            board.makeMove((PawnMove) move);
        else if (move instanceof ChangeTurnMove) {
            state.changeTurn();
        }

        if (isEndOfTurn() || (move instanceof PawnMove && !((PawnMove) move).isCapturing())) {
            changeTurn();
        }
    }

    /**
     *  Checks if the only possible move ina turn is changing turn
     * @return
     */
    private synchronized boolean isEndOfTurn() {
        ArrayList<GameMove> legalMoves = legalMoves();
        return legalMoves.size() == 1 && legalMoves.get(0) instanceof ChangeTurnMove;
    }

    public synchronized boolean isEndOfGame() {
        ArrayList<GameMove> legalMoves = legalMoves();
        return legalMoves.isEmpty();
    }

    public synchronized Optional<Color> getWinner() {
        if (board.getPlayerPawns(Color.BLACK).isEmpty()) {
            return Optional.of(Color.WHITE);
        } else if (board.getPlayerPawns(Color.WHITE).isEmpty()) {
            return Optional.of(Color.BLACK);
        } else {
            return Optional.empty();
        }
    }

    public synchronized void undoMove() {
        GameMove lastMove = state.getLastMove();

        state.removeLastMove();

        if (lastMove instanceof PawnMove) {
            board.undoMove((PawnMove) lastMove);
        }
        state.setTurn(lastMove.player);
    }

    /**
     * @return board state with set of pawns on the board
     */
    public synchronized Board.BoardState getBoardState() {
        return board.getBoardState();
    }

    public synchronized double computeRating() {
        return computeRating(state.getTurn());
    }

    public synchronized double computeRating(Color color) {
        return board.getPlayerPawns(color).size() / (double) board.getPlayerPawns(color.opposite()).size();
    }

    /**
     * @return all positions with white pawn on
     */
    public synchronized ArrayList<Position> getWhitePosition() {
        return board.pawnsPositions(board.getPlayerPawns(Color.WHITE));
    }

    /**
     * @return all positions with black pawn on
     */
    public synchronized ArrayList<Position> getBlackPosition() {
        return board.pawnsPositions(board.getPlayerPawns(Color.BLACK));
    }

    private synchronized Boolean isMoveLegal(GameMove pawnMove) {
        return legalMoves().contains(pawnMove);
    }

    /**
     * @param player
     * @return all possible moves in a current turn
     */
    public synchronized ArrayList<GameMove> legalMoves(Color player) {
        ArrayList<GameMove> moves = new ArrayList<>();
        ArrayList<PawnMove> pawnMoves;

        if (!player.equals(state.getTurn())) return moves;

        if (state.isPawnSelected()) {
            PawnMove lastPawnMove = (PawnMove) state.getLastMove();
            Pawn pawn = lastPawnMove.getPawn();
            pawnMoves = capturingMoves(pawn);

            // Remove moves in the same direction like the last one
            pawnMoves.removeIf(move -> move.direction().equals(lastPawnMove.direction()));
            // Remove moves to the same node
            pawnMoves.removeIf(move -> {
                for (GameMove previousPawnMove : state.getTurnMoves()) {
                    if (!(previousPawnMove instanceof PawnMove)) return true;
                    if (move.getTargetPosition().equals(((PawnMove) previousPawnMove).getInitialPosition()))
                        return true;
                    if (move.getTargetPosition().equals(((PawnMove) previousPawnMove).getTargetPosition())) return true;
                }
                return false;
            });

            moves.add(new ChangeTurnMove(player));
        } else {
            pawnMoves = capturingMoves(player);
            if (pawnMoves.isEmpty()) pawnMoves = nonCapturingMoves(player);
        }

        moves.addAll(pawnMoves);

        return moves;
    }

    /**
     * @param player
     * @return all capturing moves in a turn
     */
    private ArrayList<PawnMove> capturingMoves(Color player) {
        ArrayList<PawnMove> capturingPawnMoves = new ArrayList<>();
        for (Pawn pawn : board.getPlayerPawns(player)) {
            capturingPawnMoves.addAll(capturingMoves(pawn));
        }
        return capturingPawnMoves;
    }

    /**
     * @param player
     * @return all noncapturing moves in a turn
     */
    private ArrayList<PawnMove> nonCapturingMoves(Color player) {
        ArrayList<PawnMove> nonCapturingPawnMoves = new ArrayList<>();
        for (Pawn pawn : board.getPlayerPawns(player)) {
            nonCapturingPawnMoves.addAll(nonCapturingMoves(pawn));
        }
        return nonCapturingPawnMoves;
    }

    /**
     * @param pawn
     * @return all capturing moves in a turn for a particular Pawn
     */
    private ArrayList<PawnMove> capturingMoves(Pawn pawn) {
        ArrayList<PawnMove> capturingPawnMoves = potentialMoves(pawn);
        capturingPawnMoves.removeIf(move -> !move.isCapturing());
        return capturingPawnMoves;
    }

    /**
     * @param pawn
     * @return all noncapturing moves in a turn for a particular Pawn
     */
    private ArrayList<PawnMove> nonCapturingMoves(Pawn pawn) {
        ArrayList<PawnMove> nonCapturingPawnMoves = potentialMoves(pawn);
        nonCapturingPawnMoves.removeIf(move -> move.isCapturing());
        return nonCapturingPawnMoves;
    }

    /**
     * @param pawn
     * @return all moves which can be done by a particular pawn
     */
    private ArrayList<PawnMove> potentialMoves(Pawn pawn) {
        ArrayList<PawnMove> pawnMoves = new ArrayList<>();
        Node node = board.getNode(pawn);

        for (Node neighbour : board.emptyNeighbours(pawn)) {
            PawnMove pawnMove = new PawnMove(node.getPosition(), neighbour.getPosition(), pawn);

            // Capture by approach
            ArrayList<Node> nodesInDirection = board.nodesInDirection(node, pawnMove.direction());
            nodesInDirection.remove(0); // Remove target node
            ArrayList<Pawn> approachCaptured = capturePawns(pawn, nodesInDirection);

            // Capture by withdraw
            nodesInDirection = board.nodesInDirection(node, pawnMove.direction().opposite());
            ArrayList<Pawn> withdrawCaptured = capturePawns(pawn, nodesInDirection);

            if (approachCaptured.isEmpty() && withdrawCaptured.isEmpty()) {
                pawnMoves.add(pawnMove);
            } else {
                if (!approachCaptured.isEmpty()) {
                    pawnMove.addCaptured(approachCaptured, board.pawnsPositions(approachCaptured));
                    pawnMoves.add(pawnMove);
                }
                if (!withdrawCaptured.isEmpty()) {
                    if (!approachCaptured.isEmpty())
                        pawnMove = new PawnMove(node.getPosition(), neighbour.getPosition(), pawn);
                    pawnMove.addCaptured(withdrawCaptured, board.pawnsPositions(withdrawCaptured));
                    pawnMoves.add(pawnMove);
                }
            }
        }

        return pawnMoves;
    }

    /**
     * @param initialPawn
     * @param nodes
     * @return all pawns which can by captured by a particular pawn
     */
    private ArrayList<Pawn> capturePawns(Pawn initialPawn, ArrayList<Node> nodes) {
        ArrayList<Pawn> captured = new ArrayList<>();
        for (Node node : nodes) {
            Optional<Pawn> pawn = board.getPawn(node);

            if (!pawn.isPresent()) break;
            else if (pawn.get().getColor().equals(initialPawn.getColor())) break;
            else captured.add(pawn.get());
        }

        return captured;
    }

    private void changeTurn() {
        state.changeTurn();
    }

}
