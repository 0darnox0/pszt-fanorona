package model;

import java.util.*;

/**
 *  Represents state of game
 */
public class State {
    private Color turn;
    private LinkedList<GameMove> gameMoves;

    State() {
        turn = Color.WHITE;
        gameMoves = new LinkedList<>();
    }

    public Color getTurn() {
        return turn;
    }

    public void setTurn(Color turn) {
        this.turn = turn;
    }

    public void changeTurn() {
        turn = turn.opposite();
    }

    public void addMove(GameMove gameMove) {
        gameMoves.addLast(gameMove);
    }

    public GameMove getLastMove() {
        return gameMoves.getLast();
    }

    public void removeLastMove() {
        gameMoves.removeLast();
    }

    public ArrayList<GameMove> getGameMoves() {
        return new ArrayList<>(gameMoves);
    }

    /**
     * @return all possible moves to do by current player
     */
    public ArrayList<GameMove> getTurnMoves() {
        ArrayList<GameMove> turnMoves = new ArrayList<>();
        Iterator<GameMove> it = gameMoves.descendingIterator();

        while (it.hasNext()) {
            GameMove gm = it.next();
            if (!gm.player.equals(turn)) break;
            turnMoves.add(gm);
        }

        Collections.reverse(turnMoves);

        return turnMoves;
    }

    /** Checks if pawn to move is selected
     * @return
     */
    public boolean isPawnSelected() {
        try {
            return getLastMove() instanceof PawnMove && getLastMove().player.equals(turn);
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}
